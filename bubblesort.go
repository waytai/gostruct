package main

import (
    "fmt"
)

type SortInterface interface {
    sort()
}
type Sortor struct {
    name string
}

func main() {
    arry := []int{6, 1, 3, 5, 8, 4, 2, 0, 9, 7}
    learnsort := Sortor{name: "冒泡排序--从小到大--稳定--n*n---"}
    learnsort.sort(arry)
    fmt.Println(learnsort.name, arry)
}

func (sorter Sortor) sort(arry []int) {
    done := true
    arrylength := len(arry)
    for i := 0; i < arrylength && done; i++ {
        done = false
        for j := 0; j < arrylength-i-1; j++ {
            done = true
            if arry[j] > arry[j+1] {
                t := arry[j]
                arry[j] = arry[j+1]
                arry[j+1] = t
            }
        }

    }
}
